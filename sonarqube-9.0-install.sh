#!/bin/bash
#Created by Warren Roque <wroquem@gmail.com>

#https://computingforgeeks.com/install-sonarqube-code-review-centos/#
echo "Start script execution---->>sonarqube-9.0-install.sh<<----"
#"pre-requisites: must have installed Java OpenJDK 11"
#"pre-requisites: must have installed PostgreSql 13"

echo "Step 1: Configure SELinux as Permissive"
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

echo "Step 2: Tweak max_map_count and fs.file-max"
sudo sed -i '$a vm.max_map_count=262144' /etc/sysctl.conf
sudo sed -i '$a fs.file-max=65536' /etc/sysctl.conf
sudo sysctl -w vm.max_map_count=262144
sudo sysctl -w fs.file-max=65536
sudo sysctl -p

echo "Step 3: Get and Install SonarQube"
cd /opt/
sudo wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-9.0.1.46107.zip
sudo unzip sonarqube-9.0.1.46107.zip
sudo mv sonarqube-9.0.1.46107 sonarqube
sudo rm sonarqube-9.0.1.46107.zip

echo "Step 4: Create a SonarQube user and database on PostgreSql"
sudo -i -u postgres psql -c "CREATE USER sonarqube with encrypted password 'sonarqube';"
sudo -i -u postgres psql -c "CREATE DATABASE sonarqube OWNER sonarqube;"
sudo -i -u postgres psql -c "grant all privileges on database sonarqube to sonarqube;"

echo "Step 5: Configure SonarQube"
sudo sed -i '$a # My Local Configuration' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a #Setting the Access to the Database' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.jdbc.username=sonarqube' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.jdbc.password=sonarqube' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.jdbc.url=jdbc:postgresql://localhost/sonarqube' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a #Starting the Web Server' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.web.host=0.0.0.0' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.web.port=9000' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.web.context=/sonarqube' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a #Java options' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.web.javaOpts=-Xmx512m -Xms512m -XX:+HeapDumpOnOutOfMemoryError' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.search.javaOpts=-Xmx512m -Xms512m -XX:MaxDirectMemorySize=256m -XX:+HeapDumpOnOutOfMemoryError' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a #Configuring the Elasticsearch storage path' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.path.data=/opt/sonarqube/data' /opt/sonarqube/conf/sonar.properties
sudo sed -i '$a sonar.path.temp=/opt/sonarqube/temp' /opt/sonarqube/conf/sonar.properties

echo "Step 6: Create user SonarQube"
sudo useradd sonarqube
sudo passwd sonarqube
sudo chown -R sonarqube:sonarqube /opt/sonarqube

#sudo vim /opt/sonarqube/conf/wrapper.conf

echo "Step 7: Running SonarQube as a Service on Linux with SystemD"
sudo touch /etc/systemd/system/sonarqube.service
sudo echo '#Running SonarQube as a Service on Linux with SystemD' | sudo tee -a /etc/systemd/system/sonarqube.service
sudo sed -i '$a [Unit]' /etc/systemd/system/sonarqube.service
sudo sed -i '$a Description=SonarQube service' /etc/systemd/system/sonarqube.service
sudo sed -i '$a After=syslog.target network.target' /etc/systemd/system/sonarqube.service
sudo sed -i '$a [Service]' /etc/systemd/system/sonarqube.service
sudo sed -i '$a Type=simple' /etc/systemd/system/sonarqube.service
sudo sed -i '$a User=sonarqube' /etc/systemd/system/sonarqube.service
sudo sed -i '$a Group=sonarqube' /etc/systemd/system/sonarqube.service
sudo sed -i '$a PermissionsStartOnly=true' /etc/systemd/system/sonarqube.service
sudo sed -i '$a ExecStart=/bin/nohup java -Xms512m -Xmx1024m -Djava.net.preferIPv4Stack=true -jar /opt/sonarqube/lib/sonar-application-9.0.1.46107.jar' /etc/systemd/system/sonarqube.service
sudo sed -i '$a StandardOutput=syslog' /etc/systemd/system/sonarqube.service
sudo sed -i '$a LimitNOFILE=131072' /etc/systemd/system/sonarqube.service
sudo sed -i '$a LimitNPROC=8192' /etc/systemd/system/sonarqube.service
sudo sed -i '$a TimeoutStartSec=5' /etc/systemd/system/sonarqube.service
sudo sed -i '$a Restart=always' /etc/systemd/system/sonarqube.service
sudo sed -i '$a SuccessExitStatus=143' /etc/systemd/system/sonarqube.service
sudo sed -i '$a [Install]' /etc/systemd/system/sonarqube.service
sudo sed -i '$a WantedBy=multi-user.target' /etc/systemd/system/sonarqube.service

echo "Step 8: Reload, Enable and Start the service"
sudo systemctl daemon-reload
sudo systemctl enable sonarqube.service
sudo systemctl start sonarqube.service
sudo systemctl status sonarqube.service


echo "End script execution---->>sonarqube-9.0-install.sh<<----"