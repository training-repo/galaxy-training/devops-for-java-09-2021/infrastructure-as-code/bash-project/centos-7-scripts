#!/bin/bash
#Created by Warren Roque <wroquem@gmail.com>

#http://www.br8dba.com/postgresql-dba-how-to-install-postgresql-13-on-rhel-7/
#https://codepre.com/how-to-install-postgresql-13-on-centos-7.html?__cf_chl_managed_tk__=pmd_pkiBxIqfVd3b80UWkzw0clDOXyhzSAl4yEfJLUAz.HA-1633476275-0-gqNtZGzNAuWjcnBszRO9
echo "Start script execution---->>postgresql-13-install.sh<<----"

echo "Step 1: Add PostgreSQL Yum repository to CentOS 7"
sudo yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm

echo "Step 2: Install PostgreSQL 13 on CentOS 7"
sudo yum install -y postgresql13-server
echo "Step 3: Initialize and start the database service"
sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
sudo systemctl enable postgresql-13
sudo systemctl start postgresql-13
sudo systemctl status postgresql-13
echo "Step 4: Set the password of the PostgreSQL administrator user"
sudo -i -u postgres psql -c "ALTER USER postgres with password 'Admin1234';"
echo "Step 5: Enable remote database connection"
sudo sed -i.save "s/.*listen_addresses.*/listen_addresses = '*'/" /var/lib/pgsql/13/data/postgresql.conf
echo "Step 6: Configure ph_hba.conf"
sudo sed -i '$a local all all md5' /var/lib/pgsql/13/data/pg_hba.conf
sudo sed -i '$a host all all 127.0.0.1 255.255.255.255 md5' /var/lib/pgsql/13/data/pg_hba.conf
sudo sed -i '$a host all all 0.0.0.0/0 md5' /var/lib/pgsql/13/data/pg_hba.conf
sudo sed -i '$a host all all ::1/128 md5' /var/lib/pgsql/13/data/pg_hba.conf
echo "Step 7: Saving the changes, restart the database service"

sudo systemctl restart postgresql-13

echo "End script execution---->>postgresql-13-install.sh<<----"