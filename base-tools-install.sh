#!/bin/bash
#Created by Warren Roque <wroquem@gmail.com>

echo "Start script execution---->>base-tools-install.sh<<----"

echo "Update your CentOS 7 system."
sudo yum update -y 
echo "Installing epel-release"
sudo yum install epel-release -y 
echo "Installing net-tools"
sudo yum install net-tools -y
echo "Installing vim"
sudo yum install vim -y
echo "Installing curl"
sudo yum install curl -y
echo "Installing wget"
sudo yum install wget -y 
echo "Installing nano"
sudo yum install nano -y 
echo "Installing zip unzip"
sudo yum install zip unzip -y 

echo "End script execution---->>base-tools-install.sh<<----"