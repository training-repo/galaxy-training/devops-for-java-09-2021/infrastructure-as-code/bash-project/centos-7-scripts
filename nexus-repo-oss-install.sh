#!/bin/bash
#Created by Warren Roque <wroquem@gmail.com>

echo "Start script execution---->>nexus-repo-oss-install.sh<<----"
#"pre-requisites: must have installed Java OpenJDK 11"

echo "Install Nexus OSS"
sudo mkdir /opt/nexus
cd /opt/nexus
sudo wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz
sudo tar -xvf latest-unix.tar.gz
sudo rm latest-unix.tar.gz
sudo mv nexus-3.* nexus
sudo useradd nexus
sudo chown -R nexus:nexus /opt/nexus/
sudo ln -s /opt/nexus/nexus/bin/nexus /etc/init.d/nexus
cd /etc/init.d
sudo chkconfig --add nexus
sudo chkconfig --levels 345 nexus on

echo 'run_as_user="nexus"' | sudo tee -a /opt/nexus.rc
sudo mv /opt/nexus.rc /opt/nexus/nexus/bin/nexus.rc
sudo service nexus restart

echo "Waiting 2 minute ..."
sleep 2m  
echo "all Done."

echo "Print the password on your terminal:"
NEXUSPWD=$(sudo cat /opt/nexus/sonatype-work/nexus3/admin.password)
echo "The NEXUSPWD is : " $NEXUSPWD

#NEXUS_USERNAME="admin"
#NEXUS_PASSWORD="admin123"

echo "End script execution---->>nexus-repo-oss-install.sh<<----"