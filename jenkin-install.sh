#!/bin/bash
#Created by Warren Roque <wroquem@gmail.com>

echo "Start script execution---->>jenkin-install.sh<<----"
#"pre-requisites: must have installed Java OpenJDK 11"

echo "Step 1: Install Jenkins"
echo "Start by importing the repository key from Jenkins."
sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
echo "After importing the key, add the repository to the system."
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
echo "You can now start Jenkins service using:"
sudo yum install -y jenkins
export JENKINS_HOME=/var/lib/jenkins
export PATH=$PATH:$JENKINS_HOME
echo $JENKINS_HOME
sudo sed -i '$a jenkins   ALL=(ALL)   NOPASSWD:ALL' /etc/sudoers

echo "You can now start Jenkins service using:"
sudo systemctl start jenkins
sudo systemctl enable jenkins
sudo systemctl status jenkins

echo "Waiting 1 minute ..."
sleep 1m  
echo "all Done."

echo "Print the password on your terminal:"
JENKINSPWD=$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)
echo "The JENKINSPWD is : " $JENKINSPWD

#JENKINS_USERNAME="admin"
#JENKINS_PASSWORD="admin123"

echo "End script execution---->>jenkin-install.sh<<----"