#!/bin/bash
#Created by Warren Roque <wroquem@gmail.com>

echo "Start script execution---->>git-install.sh<<----"

sudo yum install -y https://packages.endpoint.com/rhel/7/os/x86_64/endpoint-repo-1.7-1.x86_64.rpm
sudo yum install -y git

echo "End script execution---->>git-install.sh<<----"